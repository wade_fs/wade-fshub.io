$(function(){
	//初始化圖片區域
	var myimg = new Image(); 
	myimg.src = $("#mypic2").attr("src"); 
	//輸出圖片數據
	$("#showSize").html(myimg.width + "×" + myimg.height);
	
	//初始化圖片的位置，根據圖片的寬度調整左右
	$("#statistics, #picArea").css("left",$(window).width()/2-myimg.width/2);
	$("#picArea").width(myimg.width).height(myimg.height);	
	var parentWidth = parseInt($("#picArea").width());
	var parentHeight = parseInt($("#picArea").height());
	
	//顯示鼠標的相對於圖片的坐標（左上角為(0,0)）
	var offsetX = parseInt($("#picArea").css("left"));
	var offsetY = parseInt($("#picArea").css("top"));
	$("#mypic").bind("mousemove",function(e){
		$("#xPos").text(e.pageX-offsetX);
		$("#yPos").text(e.pageY-offsetY);
	});
	
	var fnpicAreaDown, fnpicAreaMove, fnpicAreaUp;	//事件的函數名稱
	//點擊鼠標，出現虛線選區
	$("#picArea").bind("mousedown",fnpicAreaDown = function(e){
		var clickX = e.pageX-offsetX, clickY = e.pageY-offsetY;
		$("#selectArea").show().css({
			"left":clickX,
			"top":clickY,
			"height":"0px",
			"width":"0px"
		});
		//移動鼠標，該選區變大
		$("#picArea").bind("mousemove",fnpicAreaMove = function(e){
			//獲取鼠標移動的相對
			var iX = e.pageX-offsetX-clickX;
			var iY = e.pageY-offsetY-clickY;
			//首先判斷不能移動出picArea，兼容IE
			if(e.pageX>=offsetX && e.pageX<=offsetX+$(this).width()){
				//其次，允許從下往上拖動
				if(iX>=0)
					$("#selectArea").css("width",iX);
				else
					$("#selectArea").css({"width":-iX,"left":e.pageX-offsetX});
			}
			if(e.pageY>=offsetY && e.pageY<=offsetY+$(this).height()){
				if(iY>=0)
					$("#selectArea").css("height",iY);
				else
					$("#selectArea").css({"height":-iY,"top":e.pageY-offsetY});
			}
			moveNine();	//移動9個小方塊
			return false;	//阻止瀏覽器的預設事件
		});
		return false;	//阻止瀏覽器的預設事件
	});
	//鬆開鼠標，刪除出現選區的相關事件
	$("#picArea").bind("mouseup",fnpicAreaUp = function(e){
		$("#picArea").unbind("mousedown",fnpicAreaDown);
		$("#picArea").unbind("mousemove",fnpicAreaMove);
		cropPic();	//剪切上層圖片，實現四周陰影的效果
		$("#picArea").unbind("mouseup",fnpicAreaUp);
		return false;
	});
	
	var DivWidth, DivHeight, DivLeft, DivTop;	//選區的寬、高、左位置、上位置
	
	var fnselectDown,fnselectMove;
	//點擊移動選區，不能移出圖片本身
	$("#selectArea").bind("mousedown",fnselectDown = function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fnselectMove = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			//水平方向不能移動出去
			if(DivLeft+DivWidth+moveOffsetX>=parentWidth-2)
				$("#selectArea").css({"left":parentWidth-DivWidth-2});
			else if(DivLeft+moveOffsetX<0)
				$("#selectArea").css({"left":"0px"});
			else
				$("#selectArea").css({"left":DivLeft+moveOffsetX});
			
			//豎直方向也不能移動出去
			if(DivTop+DivHeight+moveOffsetY>=parentHeight-2)
				$("#selectArea").css({"top":parentHeight-DivHeight-2});
			else if(DivTop+moveOffsetY<0)
				$("#selectArea").css({"top":"0px"});
			else
				$("#selectArea").css({"top":DivTop+moveOffsetY});
			return false;
		});
		return false;
	});
	$("#selectArea").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fnselectMove);
		cropPic();
		return false;
	});
	
	var fn0Move;
	//左上角的小方塊
	$("#square0").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn0Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			//水平方向左移不能出圖片，右移不能把選區寬度變成負數
			if(e.pageX>=offsetX){
				if(DivLeft+moveOffsetX<=0)
					$("#selectArea").css({"left":"0px","width":DivWidth-moveOffsetX});
				else if(moveOffsetX>DivWidth-10)
					$("#selectArea").css({"left":DivLeft+DivWidth-10,"width":"10px"});
				else
					$("#selectArea").css({"left":DivLeft+moveOffsetX,"width":DivWidth-moveOffsetX});
			}
			
			//豎直方向上移不能出圖片，下移不能把選區高度變成負數
			if(e.pageY>=offsetY){
				if(DivTop+moveOffsetY<=0)
					$("#selectArea").css({"top":"0px","height":DivHeight-moveOffsetY});
				else if(moveOffsetY>DivHeight-10)
					$("#selectArea").css({"top":DivTop+DivHeight-10,"height":"10px"});
				else
					$("#selectArea").css({"top":DivTop+moveOffsetY,"height":DivHeight-moveOffsetY});
			}		
			moveNine();	//同時移動其它方塊
			return false;
		});
		return false;
	});
	$("#square0").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn0Move);
		cropPic();	//重新剪切圖片
		return false;
	});
	
	var fn1Move;
	//上面中間的小方塊
	$("#square1").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn1Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageY>=offsetY){
				if(DivTop+moveOffsetY<=0)
					$("#selectArea").css({"top":"0px","height":DivHeight-moveOffsetY});
				else if(moveOffsetY>DivHeight-10)
					$("#selectArea").css({"top":DivTop+DivHeight-10,"height":"10px"});
				else
					$("#selectArea").css({"top":DivTop+moveOffsetY,"height":DivHeight-moveOffsetY});
			}		
			moveNine();
			return false;
		});
		return false;
	});
	$("#square1").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn1Move);
		cropPic();
		return false;
	});
	
	var fn2Move;
	//右上角的小方塊
	$("#square2").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn2Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageX<=offsetX+parentWidth){
				if(DivLeft+DivWidth+moveOffsetX>=parentWidth)
					$("#selectArea").css({"width":DivWidth+moveOffsetX});
				else if(DivWidth+moveOffsetX<=10)
					$("#selectArea").css({"width":"10px"});
				else
					$("#selectArea").css({"width":DivWidth+moveOffsetX});
			}
			
			if(e.pageY>=offsetY){
				if(DivTop+moveOffsetY<=0)
					$("#selectArea").css({"top":"0px","height":DivHeight-moveOffsetY});
				else if(moveOffsetY>DivHeight-10)
					$("#selectArea").css({"top":DivTop+DivHeight-10,"height":"10px"});
				else
					$("#selectArea").css({"top":DivTop+moveOffsetY,"height":DivHeight-moveOffsetY});
			}		
			moveNine();
			return false;
		});
		return false;
	});
	$("#square2").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn2Move);
		cropPic();
		return false;
	});
		
	var fn3Move;
	//左側中間的小方塊
	$("#square3").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn3Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageX>=offsetX){
				if(DivLeft+moveOffsetX<=0)
					$("#selectArea").css({"left":"0px","width":DivWidth-moveOffsetX});
				else if(moveOffsetX>DivWidth-10)
					$("#selectArea").css({"left":DivLeft+DivWidth-10,"width":"10px"});
				else
					$("#selectArea").css({"left":DivLeft+moveOffsetX,"width":DivWidth-moveOffsetX});
			}
			moveNine();
			return false;
		});
		return false;
	});
	$("#square3").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn3Move);
		cropPic();
		return false;
	});
	
	var fn4Move;
	//右邊中間的小方塊
	$("#square4").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn4Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageX<=offsetX+parentWidth){
				if(DivLeft+DivWidth+moveOffsetX>=parentWidth)
					$("#selectArea").css({"width":DivWidth+moveOffsetX});
				else if(DivWidth+moveOffsetX<=10)
					$("#selectArea").css({"width":"10px"});
				else
					$("#selectArea").css({"width":DivWidth+moveOffsetX});
			}
			moveNine();
			return false;
		});
		return false;
	});
	$("#square4").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn4Move);
		cropPic();
		return false;
	});
	
	var fn5Move;
	//左下角的小方塊
	$("#square5").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn5Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageX>=offsetX){
				if(DivLeft+moveOffsetX<=0)
					$("#selectArea").css({"left":"0px","width":DivWidth-moveOffsetX});
				else if(moveOffsetX>DivWidth-10)
					$("#selectArea").css({"left":DivLeft+DivWidth-10,"width":"10px"});
				else
					$("#selectArea").css({"left":DivLeft+moveOffsetX,"width":DivWidth-moveOffsetX});
			}
			
			if(e.pageY<=offsetY+parentHeight){
				if(DivTop+DivHeight+moveOffsetY>=parentHeight)
					$("#selectArea").css({"height":DivHeight+moveOffsetY});
				else if(DivHeight+moveOffsetY<10)
					$("#selectArea").css({"height":"10px"});
				else
					$("#selectArea").css({"height":DivHeight+moveOffsetY});
			}		
			moveNine();
			return false;
		});
		return false;
	});
	$("#square5").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn5Move);
		cropPic();
		return false;
	});
	
	var fn6Move;
	//下面中間的小方塊
	$("#square6").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn6Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageY<=offsetY+parentHeight){
				if(DivTop+DivHeight+moveOffsetY>=parentHeight)
					$("#selectArea").css({"height":DivHeight+moveOffsetY});
				else if(DivHeight+moveOffsetY<10)
					$("#selectArea").css({"height":"10px"});
				else
					$("#selectArea").css({"height":DivHeight+moveOffsetY});
			}		
			moveNine();
			return false;
		});
		return false;
	});
	$("#square6").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn6Move);
		cropPic();
		return false;
	});
	
	var fn7Move;
	//右下角的小方塊
	$("#square7").bind("mousedown",function(e){
		var clickX = e.pageX, clickY = e.pageY;
		DivWidth = parseInt($("#selectArea").width());
		DivHeight = parseInt($("#selectArea").height());
		DivLeft = parseInt($("#selectArea").css("left"));
		DivTop = parseInt($("#selectArea").css("top"));
		$("#picArea").bind("mousemove",fn7Move = function(e){
			var moveOffsetX = e.pageX - clickX, moveOffsetY = e.pageY - clickY;
			
			if(e.pageX<=offsetX+parentWidth){
				if(DivLeft+DivWidth+moveOffsetX>=parentWidth)
					$("#selectArea").css({"width":DivWidth+moveOffsetX});
				else if(DivWidth+moveOffsetX<=10)
					$("#selectArea").css({"width":"10px"});
				else
					$("#selectArea").css({"width":DivWidth+moveOffsetX});
			}
			if(e.pageY<=offsetY+parentHeight){
				if(DivTop+DivHeight+moveOffsetY>=parentHeight)
					$("#selectArea").css({"height":DivHeight+moveOffsetY});
				else if(DivHeight+moveOffsetY<10)
					$("#selectArea").css({"height":"10px"});
				else
					$("#selectArea").css({"height":DivHeight+moveOffsetY});
			}		
			moveNine();
			return false;
		});
		return false;
	});
	$("#square7").bind("mouseup",function(e){
		$("#picArea").unbind("mousemove",fn6Move);
		cropPic();
		return false;
	});
	
	//雙擊選區切割
	$("#selectArea").bind("dblclick",function(e){
		var tempSelectArea = $(this);
		//記錄選區的四個點，用於切割
		var iCropTop = parseInt(tempSelectArea.css("top")) + 1;
		var iCropRight = parseInt(tempSelectArea.css("left")) + parseInt(tempSelectArea.width()) + 1;
		var iCropBottom = parseInt(tempSelectArea.css("top")) + parseInt(tempSelectArea.height()) + 1;
		var iCropLeft = parseInt(tempSelectArea.css("left")) + 1;
		//下層圖片剪切，final
		$("#mypic2").css("clip", "rect("+iCropTop+"px,"+iCropRight+"px,"+iCropBottom+"px,"+iCropLeft+"px)");
		//背景色變成白色
		$("#picArea").css("backgroundColor","#FFFFFF");
		tempSelectArea.hide();
	});
});

function cropPic(){
	var tempSelectArea = $("#selectArea");
	//記錄選區的四個點，用於切割
	var iCropTop = parseInt(tempSelectArea.css("top")) + 1;
	var iCropRight = parseInt(tempSelectArea.css("left")) + parseInt(tempSelectArea.width()) + 1;
	var iCropBottom = parseInt(tempSelectArea.css("top")) + parseInt(tempSelectArea.height()) + 1;
	var iCropLeft = parseInt(tempSelectArea.css("left")) + 1;
	$("#mypic1").css("clip", "rect("+iCropTop+"px,"+iCropRight+"px,"+iCropBottom+"px,"+iCropLeft+"px)");
}

function moveNine(){
	//移動那9個小方塊
	var iSelectWidth = parseInt($("#selectArea").width());
	var iSelectHeight = parseInt($("#selectArea").height());
	$("#square0").css({"left":"-1px","top":"-1px"});
	$("#square1").css({"left":iSelectWidth/2-2,"top":"-1px"});
	$("#square2").css({"left":iSelectWidth-4,"top":"-1px"});
	$("#square3").css({"left":"-1px","top":iSelectHeight/2-2});
	$("#square4").css({"left":iSelectWidth-4,"top":iSelectHeight/2-2});
	$("#square5").css({"left":"-1px","top":iSelectHeight-4});
	$("#square6").css({"left":iSelectWidth/2-2,"top":iSelectHeight-4});
	$("#square7").css({"left":iSelectWidth-4,"top":iSelectHeight-4});
	$("#square8").css({"left":iSelectWidth/2-3,"top":iSelectHeight/2-3});
	
	//這個就是給IE用的，製造一個看不見的區域來讓IE選擇
	$("#squareIE").width(Math.abs(iSelectWidth-8)).height(Math.abs(iSelectHeight-8));
}