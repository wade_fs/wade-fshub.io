<%@ Page Language="C#" ContentType="text/html" ResponseEncoding="gb2312" %>
<%@ Import Namespace="System.Data" %>
<%
	Response.CacheControl = "no-cache";
	Response.AddHeader("Pragma","no-cache");
	
	string JSON_text = "";
	switch (Request["index"]){
		case "1":
			JSON_text = "[{optionValue:1, optionDisplay: '海淀區'},{optionValue:2, optionDisplay: '東城區'},{optionValue:3, optionDisplay: '西城區'},{optionValue:4, optionDisplay: '朝陽區'},{optionValue:5, optionDisplay: '宣武區'},{optionValue:6, optionDisplay: '崇文區'},{optionValue:7, optionDisplay: '豐台區'},{optionValue:8, optionDisplay: '石景山區'},{optionValue:9, optionDisplay: '門頭溝區'},{optionValue:10, optionDisplay: '房山區'},{optionValue:11, optionDisplay: '通州區'},{optionValue:12, optionDisplay: '順義區'},{optionValue:13, optionDisplay: '大興區'},{optionValue:14, optionDisplay: '昌平區'},{optionValue:15, optionDisplay: '平谷區'},{optionValue:16, optionDisplay: '懷柔區'},{optionValue:17, optionDisplay: '密雲縣'},{optionValue:18, optionDisplay: '延慶縣'}]";
			break;
		case "2":
			JSON_text = "[{optionValue:1, optionDisplay: '黃浦區'},{optionValue:2, optionDisplay: '盧灣區'},{optionValue:3, optionDisplay: '徐匯區'},{optionValue:4, optionDisplay: '長寧區'},{optionValue:5, optionDisplay: '靜安區'},{optionValue:6, optionDisplay: '普陀區'},{optionValue:7, optionDisplay: '閘北區'},{optionValue:8, optionDisplay: '虹口區'},{optionValue:9, optionDisplay: '楊浦區'},{optionValue:10, optionDisplay: '閔行區'},{optionValue:11, optionDisplay: '寶山區'},{optionValue:12, optionDisplay: '嘉定區'},{optionValue:13, optionDisplay: '浦東新區'},{optionValue:14, optionDisplay: '金山區'},{optionValue:15, optionDisplay: '松江區'},{optionValue:16, optionDisplay: '青浦區'},{optionValue:17, optionDisplay: '南匯區'},{optionValue:18, optionDisplay: '奉賢區'}]";
			break;
		case "3":
			JSON_text = "[{optionValue:1, optionDisplay: '和平區'},{optionValue:2, optionDisplay: '河東區'},{optionValue:3, optionDisplay: '河西區'},{optionValue:4, optionDisplay: '南開區'},{optionValue:5, optionDisplay: '河北區'},{optionValue:6, optionDisplay: '紅橋區'},{optionValue:7, optionDisplay: '塘沽區'},{optionValue:8, optionDisplay: '漢沽區'},{optionValue:9, optionDisplay: '大港區'},{optionValue:10, optionDisplay: '東麗區'},{optionValue:11, optionDisplay: '西青區'},{optionValue:12, optionDisplay: '津南區'},{optionValue:13, optionDisplay: '北辰區'},{optionValue:14, optionDisplay: '武清區'},{optionValue:15, optionDisplay: '寶坻區'},{optionValue:16, optionDisplay: '薊縣'},{optionValue:17, optionDisplay: '寧河縣'},{optionValue:18, optionDisplay: '靜海縣'}]";
			break;
		default:
			JSON_text = "[{optionValue:0, optionDisplay: 'error'}]";
			break;
	}
	Response.Write(JSON_text);
%>