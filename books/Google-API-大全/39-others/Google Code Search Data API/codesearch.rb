require 'net/http'
require 'rexml/document'
# Web search for "rails"
url = 'http://www.google.com/codesearch/feeds/search?q=rails'
# get the XML data as a string
xml_data = Net::HTTP.get_response(URI.parse(url)).body
doc = REXML::Document.new(xml_data)
doc.elements.each('feed/entry') do |ele|
  puts '-----------------------'
  puts ele.elements["author"]
  puts ele.elements["title"] 
  puts ele.elements["gcs:match"] 
end
 
