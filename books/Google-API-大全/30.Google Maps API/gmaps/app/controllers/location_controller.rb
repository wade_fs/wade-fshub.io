class LocationController < ApplicationController
  def index
    @locations = Location.find(:all)
    @map = Cartographer::Gmap.new("gmaps")

    @map.controls = [ :large, :scale ]
    @map.debug = true      
    @map.center = [37.79, -122.4]   
    
    
    @locations.each do |location|     
        @map.markers << Cartographer::Gmarker.new(:name => "location_" + location.id.to_s, :position => [location.lat, location.lng], :info_window => location.description, :map => @map )
    
    end    
  end

  def create
    @location = Location.create(params[:location])
    redirect_to :action => "index"
  end

  
end


