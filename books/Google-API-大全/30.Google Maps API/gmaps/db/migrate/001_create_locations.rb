class CreateLocations < ActiveRecord::Migration
  def self.up
    create_table :locations do |t|
      t.column :address, :string, :limit => 100
      t.column :lat, :decimal, :precision => 15, :scale => 10
      t.column :lng, :decimal, :precision => 15, :scale => 10
    end
  end

  def self.down
    drop_table :locations
  end
end
