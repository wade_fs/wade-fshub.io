class AddDescription < ActiveRecord::Migration
  def self.up
    add_column :locations, :description, :string, :limit => 200
  end

  def self.down
    remove_column :locations, :description
  end
end
