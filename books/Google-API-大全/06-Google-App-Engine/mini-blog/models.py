from google.appengine.ext import db

class Entry(db.Model):
	author = db.UserProperty()
	title = db.StringProperty()
	content = db.StringProperty(multiline=True)
	create_time = db.DateTimeProperty(auto_now_add=True)
	
class Comment(db.Model):
	blog = db.ReferenceProperty(Entry)
	content = db.StringProperty(multiline=True)
	create_time = db.DateTimeProperty(auto_now_add=True)
