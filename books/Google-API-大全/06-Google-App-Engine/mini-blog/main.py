from google.appengine.ext import webapp
from google.appengine.ext.webapp.util import run_wsgi_app
import views

application = webapp.WSGIApplication([('/', views.MainPage),
									  ('/add_entry', views.AddEntry),
									  ('/view_entry', views.ViewEntry),
									  ('/login', views.Login)
                                     ],
                                     debug=True)

def main():
  run_wsgi_app(application)