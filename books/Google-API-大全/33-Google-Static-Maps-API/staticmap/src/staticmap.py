#coding:UTF-8
import os

import sys 
from PyQt4.QtCore import * 
from PyQt4.QtGui import * 
import urllib

google_key='YOU_API_KEY_HERE'

def main(): 
    app = QApplication(sys.argv) 
    w = MyWindow() 
    w.show() 
    sys.exit(app.exec_()) 
 
class MyWindow(QWidget): 
    def __init__(self, *args): 
        QWidget.__init__(self, *args) 
 
        # create objects
        label = QLabel(u"Map Address")
        self.le = QLineEdit()
        
        self.imageLabel = QLabel()

        # layout
        layout = QVBoxLayout(self)
        layout.addWidget(label)
        layout.addWidget(self.le)
        layout.addWidget(self.imageLabel)

        self.setLayout(layout) 

        # create connection
        self.connect(self.le, SIGNAL("returnPressed(void)"), self.run_command)

    def getGeoCode(self, addr):
        geo_url='http://maps.google.com/maps/geo?'+urllib.urlencode({'q':addr})+'&output=csv&key='+google_key
        try:
           g=urllib.urlopen(geo_url)
           ret=g.read().split(',')
           
           if(ret[0]!='200'):
      	      QMessageBox.warning(None, "Error", addr+" not found", QMessageBox.Yes)
           else:
      	      self.showMap(ret[2], ret[3])   
      	           
        except urllib.HTTPError:
           QMessageBox.warning(None, "Error", "Http error", QMessageBox.Yes)

    def showMap(self, lat,lang):
        stmap_url='http://maps.google.com/staticmap?center='+lat+','+lang+'&markers='+lat+','+lang+',red&zoom=14&size=512x512&maptype=mobile&key='+google_key
        urllib.urlretrieve(stmap_url, "stmap.gif")
        image = QImage("stmap.gif")
        self.imageLabel.setPixmap(QPixmap.fromImage(image))
        self.imageLabel.adjustSize()

    def run_command(self):
        addr = str(self.le.text().toUtf8())
        self.getGeoCode(addr)
  
if __name__ == "__main__": 
    main()
