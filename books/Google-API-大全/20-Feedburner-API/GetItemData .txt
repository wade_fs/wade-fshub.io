获取当前的Item信息

接口地址： 
GET feedburner.google.com/api/awareness/1.0/GetItemData?uri=<feeduri> 

示例： 
https://feedburner.google.com/api/awareness/1.0/GetItemData?uri=blogspot/tuAm 


返回数据： 
    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="ok"> 
        <feed uri="BurningQuestions"  > 
            <entry date="2005-05-10" circulation="2199" hits="39897" > 

                <item title="Google AdSense Support"  
                  url="http://blogs.feedburner.com/feedburner/archives/001162.html"  
                  itemviews="3901" clickthroughs="2500" /> 
                <item title="Premium Service: Total Stats Pro"  
                  url="http://blogs.feedburner.com/feedburner/archives/001163.html"  
                  itemviews="2003" clickthroughs="1898" /> 
                <item title="20six and Partner API"  
                  url="http://blogs.feedburner.com/feedburner/archives/001164.html" 
                  itemviews="2598" clickthroughs="2045" /> 
                <item title="Circulation Reporting and Statistics Enhancements"  
                  itemviews="598" clickthroughs="432" /> 
            </entry> 
        </feed> 

    </rsp> 

错误消息： 
   <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="1" msg="Feed Not Found" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="2" msg="This feed does not permit Awareness API access" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="4" msg="Data restricted - this feed does not have FeedBurner Stats PRO item view tracking enabled" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="5" msg="Missing required parameter (URI)" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="6" msg="Malformed parameter (DATES)" /> 
    </rsp> 


获取历史条目统计信息 
接口地址： 
GET feedburner.google.com/api/awareness/1.0/GetItemData?uri=<feeduri>&itemurl=<itemurl> &dates=2005-05-01,2005-05-02 

示例： 
GET feedburner.google.com/api/awareness/1.0/GetItemData?uri=blogspot/tuAm&itemurl=<itemurl> &dates=2005-05-01,2005-05-02 

正确返回数据： 
    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="ok"> 
        <feed uri="BurningQuestions"  > 
            <entry date="2005-05-01" circulation="2199" hits="39897" > 
                <item title="Premium Service: Total Stats Pro"   
                  url="http://blogs.feedburner.com/feedburner/archives/001162.html"  
                  itemviews="2003" clickthroughs="1898" /> 
            </entry> 
            <entry date="2005-05-02" circulation="2479" hits="40867" > 
                <item title="Premium Service: Total Stats Pro"  
                  url="http://blogs.feedburner.com/feedburner/archives/001162.html"  
                  itemviews="2453" clickthroughs="1998" /> 
            </entry> 

        </feed> 
    </rsp> 

错误返回数据： 
    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="1" msg="Feed Not Found" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="2" msg="This feed does not permit Awareness API access" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="3" msg="Item Not Found In Feed" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 

    <rsp stat="fail"> 
        <err code="4" msg="Data restricted - this feed does not have FeedBurner Stats PRO item view tracking enabled" /> 
    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="5" msg="Missing required parameter (URI)" /> 

    </rsp> 

    <?xml version="1.0" encoding="utf-8" ?> 
    <rsp stat="fail"> 
        <err code="6" msg="Malformed parameter (DATES)" /> 
    </rsp> 
