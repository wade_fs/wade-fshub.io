function initialize() { 
	// Replace data source URL on next line with your data source URL 
	var query = new google.visualization.Query('http://spreadsheets.google.com?...&key=123AB'); 
	query.send(handleQueryResponse); // Send the query with a callback function 
} 





function handleQueryResponse(response) { 
	// Called when the query response is returned 
}






function initialize() { 
	var query = new google.visualization.Query('http://spreadsheets.google.com?...&key=123AB'); 
	query.setQuery('select C, sum(B) group by C'); 
	query.send(handleQueryResponse); // Send the query with a callback function
}







function handleQueryResponse(response) { 
	if (response.isError()) { 
		alert('Error in query: ' + response.getMessage() + ' ' + response.getDetailedMessage()); 
		return; 
	} 
	
	var data = response.getDataTable(); 
	var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
	chart.draw(data, {width: 400, height: 240, is3D: true}); 
}