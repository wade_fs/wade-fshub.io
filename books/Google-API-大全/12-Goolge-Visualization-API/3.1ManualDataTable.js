var data = new google.visualization.DataTable(); 
data.addColumn('string', 'Task'); 
data.addColumn('number', 'Hours per Day'); 
data.addRows(5); 
data.setValue(0, 0, 'Work'); // Row 0, column 0 
data.setValue(0, 1, 11); // Row 0, column 1 
data.setValue(1, 0, 'Eat'); // Row 1, column 0 
data.setValue(1, 1, 2); 
data.setValue(2, 0, 'Commute'); 
data.setValue(2, 1, 2); 
data.setValue(3, 0, 'Watch TV'); 
data.setValue(3, 1, 2); 
data.setValue(4, 0, 'Sleep'); 
data.setValue(4, 1, 7);