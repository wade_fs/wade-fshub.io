//顯示消息
function message(text) {
  WScript.Echo(text);
  // 如果在IE中運行，可以使用下面的一行
  //alert(text);
}

//從註冊表中讀取已儲存的Cookie
function readCookie() {
  var cookie = 0;

  try {
    var shell = new ActiveXObject("WScript.Shell");
    cookie = shell.RegRead("HKCU\\Software\\Google\\QueryAPI_findit\\Cookie");
  } catch(e) {
  	message("Please run register_findit.js first.");
    return 0;
  }

  return cookie;
}

//進行簡單的查詢和結果處理
function findit(search_string) {
  var cookie = readCookie();

  // 初始化查詢API的接口對象
  var qapi = new ActiveXObject("GoogleDesktop.QueryAPI");

  // 執行查詢操作
  // 第3個參數是可選的，用來指定查詢類型的範圍，這裡指定在文件中
  // 第4個參數也是可選的，用來指定查詢結果的排序方式，這裡使用相關性
  var set = qapi.Query(cookie, search_string, "file", 0);

  // 處理每一個查詢結果
  var item = null;
  var i = 0;
  while (((item = set.Next()) != null) && ((i++)<3))
    message("No."+i+" URL:"+item("uri"));
}

//運行findit函數
findit("find it");