//本腳本的GUID
var g_finditGuid = "{8531E136-3E7C-4953-AC8D-35B212601165}";

//顯示消息
function message(text) {
  WScript.Echo(text);
  // 如果在IE中運行，可以使用下面的一行
  //alert(text);
}

//在註冊表中刪除已經儲存的Cookie
function deleteCookie() {
  try {
    var shell = new ActiveXObject("WScript.Shell");
    // 刪除註冊表中儲存Cookie的項目
    shell.RegDelete("HKCU\\Software\\Google\\QueryAPI_findit\\");
  } catch (e) {
  	message("Can't delete Cookie.");
    return false;
  }

  return true;
}

//取消註冊
function unregister() {
  try {
    var registrar = new ActiveXObject("GoogleDesktop.Registrar");
    registrar.UnregisterComponent(g_finditGuid);
  } catch (e) {
  	message("Can't unregister.");
    return false;
  }

  return true;
}

//主函數
function main() {
  unregister();
  deleteCookie();
  message("Completed unregistration and deleted the cookie.");
}

//運行main函數
main();
