//本腳本的GUID
var g_finditGuid = "{2F375D32-944F-4154-9E76-AE1737B8965E}";

//顯示消息
function message(text) {
  WScript.Echo(text);
  //如果在IE中運行，可以使用下面的一行
  //alert(text);
}

//把Cookie儲存到註冊表中
function storeCookie(cookie) {
  var shell = new ActiveXObject("WScript.Shell");
  shell.RegWrite("HKEY_CURRENT_USER\\Software\\Google\\QueryAPI_findit\\Cookie", cookie, "REG_DWORD");
}

//從註冊表中讀取已儲存的Cookie
function readCookie() {
  var cookie = 0;

  try {
    var shell = new ActiveXObject("WScript.Shell");
    cookie = shell.RegRead("HKEY_CURRENT_USER\\Software\\Google\\QueryAPI_findit\\Cookie");
  } catch(e) {
    return 0;
  }

  return cookie;
}

//註冊，返回得到的Cookie
function register(read_only) {
  var descArray = new Array();
  descArray[0] = "Title"; // 標題
  descArray[1] = "Query API examples Findit";
  descArray[2] = "Description"; // 描述
  descArray[3] = "for demonstration purposes.";
  descArray[4] = "Icon"; // 圖標
  descArray[5] = "test Icon@1";

  var cookie = 0;

  try {
    // 開始註冊
    var registrar = new ActiveXObject("GoogleDesktop.Registrar");

    // 提供我們的GUID和描述
    registrar.StartComponentRegistration(g_finditGuid, descArray);

    // 獲取查詢API的接口對象
    var regObj = registrar.GetRegistrationInterface("GoogleDesktop.QueryRegistration");

    // 向查詢API的接口對象註冊我們的腳本，並獲取Cookie
    cookie = regObj.RegisterPlugin(g_finditGuid, read_only);

    // 完成註冊
    registrar.FinishComponentRegistration();
  } catch (e) {
    message("Couldn't register Findit.\n" + new String(e.number) +
            "\n" + e.description);
    return 0;
  }

  return cookie;
}

//主函數
function main() {
  // 檢查是否已經註冊過
  var existingCookie = readCookie();
  if (existingCookie == 0) {
    // 註冊我們的腳本Findit
    var readOnlyAccess = false;
    var registrationCookie = register(readOnlyAccess);
    if (registrationCookie != 0) {
      // 儲存Cookie
      storeCookie(registrationCookie);
    }
  } else {
    message("Has registered. You can unregister first.");
  }
}

//運行main函數
main();