var feed = new google.feeds.Feed("http://www.google-apis.com/rss/index.xml");
feed.load(function(result) {
 if (!result.error) {
 var container = document.getElementById("feed");
 for (var i = 0; i < result.feed.entries.length; i++) {
 var entry = result.feed.entries[i];
 var attributes = ["title", "link", "publishedDate", "contentSnippet"];    //標準的Atom格式屬性
 for (var j = 0; j < attributes.length; j++) {
 var div = document.createElement("div");
 div.appendChild(document.createTextNode(entry[attributes[j]]));
 container.appendChild(div);
 }
 }
 }
});