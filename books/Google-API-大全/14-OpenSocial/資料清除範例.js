//清除一個單一的key和value代碼如下：

var req = opensocial.newDataRequest();
req.add(req.newRemovePersonAppDataRequest("VIEWER", "score"), "clear_data");
req.send(set_callback);

//跟取得資訊類似，如果想要清除多個key的資訊同樣使用數組來表示：

var req = opensocial.newDataRequest();
req.add(req.newRemovePersonAppDataRequest("VIEWER", ["score", "time"]), "clear_data");
req.send(set_callback);