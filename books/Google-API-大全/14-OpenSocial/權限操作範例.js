	  /**
       * 判斷該應用是否有權限訪問查看者的資訊， 如果沒有則請求權限。
       */
      function requestPermission() {
        if (opensocial.hasPermission(opensocial.Permission.VIEWER)) {
          requestData();
        } else {
          var reason = "顯示您好友的比分";

          opensocial.requestPermission(opensocial.Permission.VIEWER, reason, onPermissionRequested);
        }
      };

      /**
       * 處理請求權限的返回資訊，如果沒有錯誤說明請求成功。如果返回的錯誤code為NOT_IMPLEMENTED，說明該容器沒有實現請求權限的操作。
       */
      function onPermissionRequested(response) {
        if (response.hadError()) {
          switch(response.getErrorCode()) {
            case opensocial.ResponseItem.Error.NOT_IMPLEMENTED :
              alert("該容器沒有實現請求權限的操作");
              break;

            default:
              alert("請求權限時產生了錯誤: " + response.getErrorMessage());
              break;
          };
        }
        requestData();

      };

      /**
       * 擁有權限後進行資訊的讀取操作.
       */
      function requestData() {
        var req = opensocial.newDataRequest();
        req.add(req.newFetchPersonRequest(opensocial.IdSpec.PersonId.OWNER), "owner");

        //判斷是否有權限。
        if (opensocial.hasPermission(opensocial.Permission.VIEWER)) {
          req.add(req.newFetchPersonRequest(opensocial.IdSpec.PersonId.VIEWER), "viewer");

        }
        req.send(showData);
      };

      /**
       * 回調函數處理返回的資訊。
       */
      function showData(data) {
        var owner = data.get("owner").getData();
        var ownerOutput = document.getElementById("owner-output");

        showPerson(owner, ownerOutput);
        if (opensocial.hasPermission(opensocial.Permission.VIEWER)) {
          var viewer = data.get("viewer").getData();
          var viewerOutput = document.getElementById("viewer-output");
          showPerson(viewer, viewerOutput);
        }
      }

      /**
       * 顯示個人資訊。
       */
      function showPerson(person, div) {
        var name = person.getDisplayName();
        var thumb = person.getField(opensocial.Person.Field.THUMBNAIL_URL);
        var html = '<img src="' + thumb + '"/>' + name;
        div.innerHTML = html;
      }

      //頁面載入的時候調用requestPermission方法，首先判斷是否有操作的權限
      gadgets.util.registerOnLoadHandler(requestPermission);