using System;
using System.Collections.Generic;
using System.Text;
namespace QuoteReader
{
    /// <summary>
    /// Business Entity
    /// </summary>
    public interface IQuote
    {
        /// <summary>
        /// 名稱
        /// </summary>
        string Name { get;}

        /// <summary>
        /// 單價
        /// </summary>
        string UnitPrice { get;}

        /// <summary>
        /// 庫存
        /// </summary>
        string QuantityInStock { get;}

        /// <summary>
        /// 加密
        /// </summary>
        void Encrypt();

        /// <summary>
        /// 解密
        /// </summary>
        void Decrypt();

        /// <summary>
        /// 值
        /// </summary>
        string[] Values { get;}
    }
}
