using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MarvellousWorks.Training.GSpreadsheets;
namespace QuoteReader
{
    public partial class AddQuoteForm : Form
    {
        #region Windows Code
        public AddQuoteForm()
        {
            InitializeComponent();
        }
        #endregion

        /// <summary>
        /// 當前的資訊
        /// </summary>
        public IQuote Data
        {
            get
            {
                return new Quote(txtName.Text,
                    txtUnitPrice.Text, txtQuantityInStock.Text);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            this.Close();
            this.Visible = false;
        }
    }
}