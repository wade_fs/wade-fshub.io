using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.IO;
namespace QuoteReader
{
    /// <summary>
    /// 處理常規的密碼操作，包括：加密/解密/散列/散列驗證操作
    /// </summary>
    /// <remarks>
    ///     <list type="bullet">
    ///         <item>採用標準DES算法進行基於固定密鑰的加密/解密處理</item>
    ///         <item>採用標準SHA1算法計算散列</item>
    ///     </list>
    /// </remarks>
    static class CryptoHelper
    {
        #region Private field
        /// <summary>
        /// Crypto transformer of encryption
        /// </summary>
        private static ICryptoTransform et;

        /// <summary>
        /// Crypto transformer of decryption
        /// </summary>
        private static ICryptoTransform dt;
        #endregion

        #region Constructor
        /// <summary>
        /// 構造函數。
        /// </summary>
        /// <remarks>無參數的構造函數，用預設的方式初始化各個屬性。
        /// </remarks>
        static CryptoHelper()
        {
            DESCryptoServiceProvider cryptoService = new DESCryptoServiceProvider();
            byte[] cryptoKey = { 136, 183, 142, 217, 175, 71, 90, 239 };
            byte[] cryptoIV = { 227, 105, 5, 40, 162, 158, 143, 156 };


            cryptoService.Key = cryptoKey;
            cryptoService.IV = cryptoIV;

            CryptoHelper.et = cryptoService.CreateEncryptor();
            CryptoHelper.dt = cryptoService.CreateDecryptor();
        }
        #endregion

        #region nvative key encrypt / decrypt

        /// <summary>
        /// 對字符串的解密處理
        /// </summary>
        /// <param name="source">需要解密的字符串</param>
        /// <returns>解密後的字符串</returns>
        /// <remarks>對字符串的解密處理
        /// </remarks>
        public static string Decrypt(string source)
        {
            byte[] buff = Convert.FromBase64String(source);
            using (MemoryStream mem = new MemoryStream())
            {
                using (CryptoStream stream = new CryptoStream(mem, CryptoHelper.dt, CryptoStreamMode.Write))
                {
                    stream.Write(buff, 0, buff.Length);
                    stream.Close();
                }
                return Encoding.Unicode.GetString(mem.ToArray());
            }
        }

        /// <summary>
        /// 對字符串的加密處理
        /// </summary>
        /// <param name="source">需要加密的字符串</param>
        /// <returns>加密後的字符串</returns>
        /// <remarks>對字符串的加密處理
        /// </remarks>
        public static string Encrypt(string source)
        {
            byte[] buff = Encoding.Unicode.GetBytes(source);

            MemoryStream mem = new MemoryStream();
            CryptoStream stream = new CryptoStream(mem, CryptoHelper.et, CryptoStreamMode.Write);
            stream.Write(buff, 0, buff.Length);
            stream.FlushFinalBlock();
            stream.Clear();

            return Convert.ToBase64String(mem.ToArray());
        }
        #endregion
    }
}
