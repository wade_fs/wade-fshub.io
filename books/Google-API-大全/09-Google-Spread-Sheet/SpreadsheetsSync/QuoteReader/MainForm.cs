using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using MarvellousWorks.Training.GSpreadsheets;
namespace QuoteReader
{
    public partial class MainForm : Form
    {
        #region Windows Code
        public MainForm()
        {
            InitializeComponent();
        }
        #endregion

        private const string SpreadsheetTitle = "MarvellousWorks";
        private const string WorksheetTitle = "Quote";
        private readonly string[] fields = new string[]{
            "Name", "UnitPrice", "QuantityInStock"};
        private QuoteCollection collection;

        /// <summary>
        /// 初始化Google Spreadsheet連接
        /// </summary>
        private GSpreadsheetsGateway CreateGateway()
        {
            return new GSpreadsheetsGateway("marvellousworks", "<google account>", "<password>");
        }

        /// <summary>
        /// 是否可以連接到Google Spreadsheet
        /// </summary>
        private bool IsConnected
        {
            get { return Convert.ToBoolean(ConfigurationManager.AppSettings["IsConnected"]); }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            AddQuoteForm form = new AddQuoteForm();
            form.ShowDialog();
            IQuote current = form.Data;

            // 並沒有錄入新的資訊則直接退出
            if (string.IsNullOrEmpty(current.Name) && string.IsNullOrEmpty(current.Name)
                && string.IsNullOrEmpty(current.QuantityInStock))
                return;

            if (!IsConnected)
            {
                MessageBox.Show("Cannot connect to Google Spreadsheet");
                return;
            }

            // 提交
            current.Encrypt();
            GSpreadsheetsGateway gateway = CreateGateway();
            gateway.AddRow(SpreadsheetTitle, WorksheetTitle, fields, current.Values);

            // 更新UI
            collection.AddRow(current.Values);
            grdQuote.DataSource = collection.Data;
            form = null;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // 在線方式
            if (IsConnected)
            {
                GSpreadsheetsGateway gateway = CreateGateway();
                DataTable result = gateway.GetWorksheetContent(
                    SpreadsheetTitle, WorksheetTitle);
                collection = new QuoteCollection(result);   // 構造並解密資訊
            }
            // 離線方式
            else
            {
                if (!File.Exists(QuoteCollection.LocalFileName))
                    return;
                collection = new QuoteCollection();
                collection.LoadFromFile();
            }
            grdQuote.DataSource = collection.Data;      // 綁定到UI
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            collection.SaveToFile();
        }
    }
}