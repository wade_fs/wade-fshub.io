using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization.Formatters.Soap;
using System.Runtime.Remoting.Messaging;
namespace QuoteReader
{
    /// <summary>
    /// 幫助對象實現序列化和反序列化。
    /// </summary>
    /// <remarks>對象序列化是把對象序列化轉化為string類型；對象反序列化是把對象從string類型反序列化轉化為其源類型。
    /// </remarks>
    static class SerializationHelper
    {
        /// <summary>
        /// 把對象序列化轉換為字符串
        /// </summary>
        /// <param name="graph">可串行化對象實例</param>
        /// <param name="formatterType">消息格式編碼類型（Soap或Binary型）</param>
        /// <returns>串行化轉化結果</returns>
        /// <remarks>調用BinaryFormatter或SoapFormatter的Serialize方法實現主要轉換過程。
        /// </remarks>
        public static string SerializeObjectToString(object graph)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                IRemotingFormatter formatter = new SoapFormatter();
                formatter.Serialize(memoryStream, graph);
                Byte[] arrGraph = memoryStream.ToArray();
                return Convert.ToBase64String(arrGraph);
            }
        }

        /// <summary>
        /// 把已序列化為字符串類型的對象反序列化為指定的類型
        /// </summary>
        /// <param name="serializedGraph">已序列化為字符串類型的對象</param>
        /// <param name="formatterType">消息格式編碼類型（Soap或Binary型）</param>
        /// <typeparam name="T">對象轉換後的類型</typeparam>
        /// <returns>串行化轉化結果</returns>
        /// <remarks>調用BinaryFormatter或SoapFormatter的Deserialize方法實現主要轉換過程。
        /// </remarks>
        public static T DeserializeStringToObject<T>(string serializedGraph)
        {
            Byte[] arrGraph = Convert.FromBase64String(serializedGraph);
            using (MemoryStream memoryStream = new MemoryStream(arrGraph))
            {
                IRemotingFormatter formatter = new SoapFormatter();
                return (T)formatter.Deserialize(memoryStream);
            }
        }
    }
}
