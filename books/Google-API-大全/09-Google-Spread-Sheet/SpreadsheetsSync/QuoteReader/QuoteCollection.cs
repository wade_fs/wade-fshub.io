using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.IO;
using MarvellousWorks.Training.GSpreadsheets;
namespace QuoteReader
{
    /// <summary>
    /// 訂單資訊容器
    /// </summary>
    public class QuoteCollection
    {
        private DataTable table;
        public const string LocalFileName = "Quote.txt";

        #region Constructors

        /// <summary>
        /// 構造
        /// </summary>
        /// <param name="table"></param>
        /// <remarks>獲得從Google Spreadsheet反饋的資訊</remarks>
        public QuoteCollection(DataTable table)
        {
            this.table = CloneDataTable(table, OperationType.Decrypt);
        }

        /// <summary>
        /// 構造
        /// </summary>
        /// <remarks>離線方式的構造函數</remarks>
        public QuoteCollection()
        {
        }

        #endregion

        /// <summary>
        /// 索引器
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public IQuote this[int index]
        {
            get
            {
                DataRow row = table.Rows[index];
                return new Quote(row);
            }
        }

        /// <summary>
        /// 序列化並持久化到文件
        /// </summary>
        /// <returns></returns>
        public void SaveToFile()
        {
            DataTable clone = CloneDataTable(this.table, OperationType.Encrypt);
            string graph = SerializationHelper.SerializeObjectToString(clone);
            File.WriteAllText(LocalFileName, graph);
        }

        /// <summary>
        /// 從文件反序列化
        /// </summary>
        public void LoadFromFile()
        {
            string graph = File.ReadAllText(LocalFileName);
            DataTable data = SerializationHelper.DeserializeStringToObject<DataTable>(graph);
            this.table = CloneDataTable(data, OperationType.Decrypt);
        }

        /// <summary>
        /// 資訊
        /// </summary>
        public DataTable Data
        {
            get { return this.table; }
        }

        /// <summary>
        /// 增加新的記錄
        /// </summary>
        /// <param name="values"></param>
        public void AddRow(string[] values)
        {
            table.Rows.Add(values);
        }

        #region Helper Methods

        enum OperationType
        {
            Encrypt,
            Decrypt
        }

        /// <summary>
        /// 根據操作類型獲得資訊的副本
        /// </summary>
        /// <param name="data"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private DataTable CloneDataTable(DataTable data, OperationType type)
        {
            if (data == null) return null;
            DataTable clone = data.Clone();
            foreach (DataRow row in data.Rows)
            {
                IQuote quote = new Quote(row);
                if (type == OperationType.Encrypt)
                    quote.Encrypt();
                else
                    quote.Decrypt();
                clone.Rows.Add(quote.Name, quote.UnitPrice, quote.QuantityInStock);
            }
            return clone;
        }

        #endregion
    }
}
