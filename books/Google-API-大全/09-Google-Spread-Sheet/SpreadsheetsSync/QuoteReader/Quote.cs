using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
namespace QuoteReader
{
    class Quote : IQuote
    {
        #region Public Constants

        public const string NameItem = "Name";
        public const string UnitPriceItem = "UnitPrice";
        public const string QuantityInStockItem = "QuantityInStock";

        #endregion

        #region Private Fields

        private string name;
        private string unitPrice;
        private string quantityInStock;

        #endregion

        #region Constructors
        /// <summary>
        /// �c�y
        /// </summary>
        /// <param name="data"></param>
        public Quote(DataRow data)
        {
            if (data == null)
                throw new ArgumentNullException("data");
            name = Convert.ToString(data[NameItem]);
            unitPrice = Convert.ToString(data[UnitPriceItem]);
            quantityInStock = Convert.ToString(data[QuantityInStockItem]);
        }

        /// <summary>
        /// �c�y
        /// </summary>
        /// <param name="name"></param>
        /// <param name="unitPrice"></param>
        /// <param name="quantityInStock"></param>
        public Quote(string name, string unitPrice, string quantityInStock)
        {
            this.name = name;
            this.unitPrice = unitPrice;
            this.quantityInStock = quantityInStock;
        }
        #endregion

        #region IQuote Members

        /// <summary>
        /// <see cref="IQuote"/>
        /// </summary>
        public string Name
        {
            get { return this.name; }
        }

        /// <summary>
        /// <see cref="IQuote"/>
        /// </summary>
        public string UnitPrice
        {
            get { return this.unitPrice; }
        }

        /// <summary>
        /// <see cref="IQuote"/>
        /// </summary>
        public string QuantityInStock
        {
            get { return this.quantityInStock; }
        }

        /// <summary>
        /// <see cref="IQuote"/>
        /// </summary>
        public string[] Values
        {
            get{return new string[]{name, unitPrice, quantityInStock};}
        }

        /// <summary>
        /// <see cref="IQuote"/>
        /// </summary>
        public void Encrypt()
        {
            this.name = CryptoHelper.Encrypt(this.name);
            this.unitPrice = CryptoHelper.Encrypt(this.unitPrice);
            this.quantityInStock = CryptoHelper.Encrypt(this.quantityInStock);
        }

        /// <summary>
        /// <see cref="IQuote"/>
        /// </summary>
        public void Decrypt()
        {
            this.name = CryptoHelper.Decrypt(this.name);
            this.unitPrice = CryptoHelper.Decrypt(this.unitPrice);
            this.quantityInStock = CryptoHelper.Decrypt(this.quantityInStock);
        }

        #endregion
    }
}
