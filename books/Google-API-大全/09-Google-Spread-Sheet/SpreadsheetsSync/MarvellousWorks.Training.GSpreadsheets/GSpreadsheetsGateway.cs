using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.GData.Spreadsheets;
using System.Net;
using System.Diagnostics;
namespace MarvellousWorks.Training.GSpreadsheets
{
    /// <summary>
    /// 訪問 Google Spreadsheets API 的 Gateway
    /// </summary>
    /// <remarks>
    /// <list type="bulletin">
    ///     <item>每次查詢獲得實體的次序不定</item>
    /// </list>
    /// </remarks>
    public class GSpreadsheetsGateway
    {
        #region Private Fields

        /// <summary>
        /// 連接到Google Spreadsheets服務的對象實例
        /// </summary>
        private SpreadsheetsService service;

        #endregion

        #region Constructors

        /// <summary>
        /// 構造
        /// </summary>
        /// <param name="serviceName">
        /// 連接到Google Spreadsheet服務的邏輯名稱
        /// <remarks>
        ///     按照Google的建議一般採用如下形式：
        ///     companyName-applicationName-versionID
        ///     <example>marvellous-pet-1</example>
        /// </remarks>
        /// </param>
        /// <param name="userName">帳號</param>
        /// <param name="password">密碼</param>
        public GSpreadsheetsGateway(
            string serviceName, string userName, string password)
        {
            if(string.IsNullOrEmpty(serviceName))
                throw new ArgumentNullException("serviceName");
            if (string.IsNullOrEmpty(userName))
                throw new ArgumentNullException("userName");

            // 連接
            service = new SpreadsheetsService(serviceName);
            service.setUserCredentials(userName, password);
        }

        #endregion

        #region Public Methods Spreadsheet

        /// <summary>
        /// 查詢並獲得所有SpreadsheetEntry實體列表
        /// </summary>
        /// <returns>SpreadsheetEntry實體列表</returns>
        public IDictionary<string, SpreadsheetEntry> GetSpreadsheets()
        {
            SpreadsheetQuery query = new SpreadsheetQuery();
            SpreadsheetFeed feed = service.Query(query);
            return ConvertCollection<SpreadsheetEntry>(feed.Entries);
        }

        /// <summary>
        /// 根據標題獲得指定的SpreadsheetEntry
        /// </summary>
        /// <param name="title">標題</param>
        /// <returns>SpreadsheetEntry</returns>
        public SpreadsheetEntry GetSpreadsheet(string title)
        {
            SpreadsheetQuery query = new SpreadsheetQuery();
            query.Title = title;
            SpreadsheetFeed feed = service.Query(query);
            return (SpreadsheetEntry)feed.Entries[0];
        }


        /// <summary>
        /// 獲得整個SpreadsheetEntry的內容，並將它封裝為DataSet返回
        /// </summary>
        /// <param name="entry">SpreadsheetEntry</param>
        /// <returns>內容</returns>
        /// <remarks>
        /// 為了便於使用，採用DataTable作為返回結果<br/>
        /// 由於ColCount和Cols屬性返回的內容要大於實際儲存了資訊的範圍，
        /// 因此列數基於Google電子表格服務反饋的XML資訊為準<br/>
        /// </remarks>
        public DataSet GetSpreadsheetContent(SpreadsheetEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            DataSet result = new DataSet(entry.Title.Text);
            IDictionary<string, WorksheetEntry> worksheets =
                GetWorksheets(entry);
            if ((worksheets != null) && (worksheets.Count > 0))
            {
                foreach (WorksheetEntry worksheet in worksheets.Values)
                {
                    DataTable table = GetWorksheetContent(worksheet);
                    if (table != null)
                        result.Tables.Add(table);
                }
            }
            return result;
        }

        #endregion

        #region Public Methods Worksheets

        /// <summary>
        /// 查詢並獲得某個Spreadsheet的WorksheetEntry列表
        /// </summary>
        /// <param name="entry">SpreadsheetEntry</param>
        /// <returns>WorksheetEntry列表</returns>
        public IDictionary<string, WorksheetEntry>
            GetWorksheets(SpreadsheetEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            AtomLink link = entry.Links.FindService(
                GDataSpreadsheetsNameTable.WorksheetRel, null);
            WorksheetQuery query = new WorksheetQuery(link.HRef.ToString());
            WorksheetFeed feed = service.Query(query);
            return ConvertCollection<WorksheetEntry>(feed.Entries);
        }

        /// <summary>
        /// 查詢所有WorksheetEntry列表
        /// </summary>
        /// <returns></returns>
        public IDictionary<string, WorksheetEntry> GetAllWorksheets()
        {
            IDictionary<string, SpreadsheetEntry> spreadsheets =
                GetSpreadsheets();
            if ((spreadsheets == null) || (spreadsheets.Count == 0))
                return null;
            Dictionary<string, WorksheetEntry> result =
                new Dictionary<string, WorksheetEntry>();
            foreach (SpreadsheetEntry spreadsheet in spreadsheets.Values)
            {
                IDictionary<string, WorksheetEntry> worksheets =
                    GetWorksheets(spreadsheet);
                if (worksheets != null)
                {
                    foreach (WorksheetEntry worksheet in worksheets.Values)
                        result.Add(worksheet.Title.Text, worksheet);
                }
            }
            return result;
        }

        /// <summary>
        /// 獲得某個Spreadsheet下指定標題的Worksheet
        /// </summary>
        /// <param name="entry">SpreadsheetEntry</param>
        /// <param name="title">標題</param>
        /// <returns>WorksheetEntry</returns>
        public WorksheetEntry GetWorksheet(SpreadsheetEntry entry, string title)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            if (string.IsNullOrEmpty(title))
                throw new ArgumentNullException("title");
            AtomLink link = entry.Links.FindService(
                GDataSpreadsheetsNameTable.WorksheetRel, null);
            WorksheetQuery query = new WorksheetQuery(link.HRef.ToString());
            query.Title = title;
            WorksheetFeed feed = service.Query(query);
            if ((feed == null) || (feed.Entries == null) || (feed.Entries.Count == 0))
                return null;
            else
                return (WorksheetEntry)feed.Entries[0];
        }

        /// <summary>
        /// 獲得某個Spreadsheet下指定標題的Worksheet
        /// </summary>
        /// <param name="spreadsheetTitle">Spreadsheet標題</param>
        /// <param name="worksheetTitle">Worksheet標題</param>
        /// <returns>WorksheetEntry</returns>
        public WorksheetEntry GetWorksheet(
            string spreadsheetTitle, string worksheetTitle)
        {
            if (string.IsNullOrEmpty(spreadsheetTitle))
                throw new ArgumentNullException("spreadsheetTitle");
            if (string.IsNullOrEmpty(worksheetTitle))
                throw new ArgumentNullException("worksheetTitle");
            SpreadsheetEntry entry = GetSpreadsheet(spreadsheetTitle);
            if (entry == null)
                return null;
            return GetWorksheet(entry, worksheetTitle);
        }

        /// <summary>
        /// 為Spreadsheet增加一個新的Worksheet
        /// </summary>
        /// <param name="entry">SpreadsheetEntry</param>
        /// <param name="title">Worksheet標題</param>
        /// <param name="rows">行數</param>
        /// <param name="cols">列數</param>
        /// <returns>WorksheetEntry</returns>
        public WorksheetEntry AddWorksheet(SpreadsheetEntry entry,
            string title, uint rows, uint cols)
        {
            AtomLink link = entry.Links.FindService(
                GDataSpreadsheetsNameTable.WorksheetRel, null);
            WorksheetQuery query = new WorksheetQuery(link.HRef.ToString());
            WorksheetFeed feed = service.Query(query);
            foreach (WorksheetEntry worksheet in feed.Entries)
            {

                if (string.Equals(worksheet.Title.Text, title, StringComparison.InvariantCulture))
                {
                    throw new DuplicateNameException("title");
                }
            }

            WorksheetEntry newWorksheet =
                new WorksheetEntry(rows, cols, title);
            WorksheetEntry result = (WorksheetEntry)feed.Insert(newWorksheet);
            return result;
        }

        /// <summary>
        /// 獲得整個Worksheet的內容
        /// </summary>
        /// <param name="entry">WorksheetEntry</param>
        /// <returns>內容</returns>
        /// <remarks>
        /// 為了便於使用，採用DataTable作為返回結果<br/>
        /// 由於ColCount和Cols屬性返回的內容要大於實際儲存了資訊的範圍，
        /// 因此列數基於Google電子表格服務反饋的XML資訊為準<br/>
        /// </remarks>
        public DataTable GetWorksheetContent(WorksheetEntry entry)
        {
            if (entry == null)
                return null;
            AtomLink listFeedLink = entry.Links.FindService(
                GDataSpreadsheetsNameTable.ListRel, null);
            ListQuery query = new ListQuery(listFeedLink.HRef.ToString());
            ListFeed feed = service.Query(query);
            return ConvertListFeedToDataTable(entry.Title.Text, feed);
        }

        /// <summary>
        /// 獲得整個Worksheet的內容
        /// </summary>
        /// <param name="spreadsheetTitle">Spreadsheet標題</param>
        /// <param name="worksheetTitle">Worksheet標題</param>
        /// <returns>DataTable</returns>
        public DataTable GetWorksheetContent(
            string spreadsheetTitle, string worksheetTitle)
        {
            WorksheetEntry entry = GetWorksheet(spreadsheetTitle, worksheetTitle);
            if (entry == null)
                return null;
            return GetWorksheetContent(entry);
        }

        /// <summary>
        /// 向Worksheet追加記錄
        /// </summary>
        /// <param name="entry">WorksheetEntry</param>
        /// <param name="parameters">
        /// 新記錄的各個內容項
        /// </param>
        public void AddRow(WorksheetEntry entry,
            KeyValuePair<string, string>[] parameters)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            if((parameters == null) || (parameters.Length == 0))
                throw new ArgumentNullException("parameters");

            AtomLink listFeedLink = entry.Links.FindService(
                GDataSpreadsheetsNameTable.ListRel, null);
            ListQuery query = new ListQuery(listFeedLink.HRef.ToString());
            ListFeed feed = service.Query(query);
            ListEntry newRow = new ListEntry();
            foreach (KeyValuePair<string, string> parameter in parameters)
            {
                ListEntry.Custom element = new ListEntry.Custom();
                element.LocalName = parameter.Key;
                element.Value = parameter.Value;
                newRow.Elements.Add(element);
            }
            feed.Insert(newRow);
        }

        /// <summary>
        /// 向Worksheet追加記錄
        /// </summary>
        /// <param name="entry">WorksheetEntry</param>
        /// <param name="localNames">內容項名稱列表</param>
        /// <param name="values">內容項的值列表</param>
        public void AddRow(WorksheetEntry entry, string[] localNames, string[] values)
        {
            if ((localNames == null) || (localNames.Length == 0))
                throw new ArgumentNullException("localNames");
            if ((values == null) || (values.Length == 0))
                throw new ArgumentNullException("values");
            if (localNames.Length != values.Length)
                throw new IndexOutOfRangeException("values");

            KeyValuePair<string, string>[] pairs = new KeyValuePair<string, string>[localNames.Length];
            for (int i = 0; i < pairs.Length; i++)
                pairs[i] = new KeyValuePair<string, string>(localNames[i], values[i]);
            AddRow(entry, pairs);
        }

        /// <summary>
        /// 向某個Spreadsheet下指定標題的Worksheet追加記錄
        /// </summary>
        /// <param name="spreadsheetTitle">Spreadsheet標題</param>
        /// <param name="worksheetTitle">Worksheet標題</param>
        /// <param name="parameters">新記錄的各個內容項</param>
        public void AddRow(string spreadsheetTitle, string worksheetTitle,
            KeyValuePair<string, string>[] parameters)
        {
            if (string.IsNullOrEmpty(spreadsheetTitle))
                throw new ArgumentNullException("spreadsheetTitle");
            if (string.IsNullOrEmpty(worksheetTitle))
                throw new ArgumentNullException("worksheetTitle");
            WorksheetEntry entry = GetWorksheet(spreadsheetTitle, worksheetTitle);
            AddRow(entry, parameters);
        }

        /// <summary>
        /// 向某個Spreadsheet下指定標題的Worksheet追加記錄
        /// </summary>
        /// <param name="spreadsheetTitle">Spreadsheet標題</param>
        /// <param name="worksheetTitle">Worksheet標題</param>
        /// <param name="localNames">內容項名稱列表</param>
        /// <param name="values">內容項的值列表</param>
        public void AddRow(string spreadsheetTitle, string worksheetTitle,
            string[] localNames, string[] values)
        {
            if (string.IsNullOrEmpty(spreadsheetTitle))
                throw new ArgumentNullException("spreadsheetTitle");
            if (string.IsNullOrEmpty(worksheetTitle))
                throw new ArgumentNullException("worksheetTitle");
            WorksheetEntry entry = GetWorksheet(spreadsheetTitle, worksheetTitle);
            AddRow(entry, localNames, values);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// 將非泛型的ICollection轉換為泛型的IDictionary<string, T>;
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="collection">查詢獲得的實體集合</param>
        /// <returns>轉換後的結果</returns>
        private static IDictionary<string, T> ConvertCollection<T>(AtomEntryCollection collection)
            where T : AtomEntry
        {
            if (collection == null)
                return null;
            Dictionary<string, T> result = new Dictionary<string, T>();
            foreach (object item in collection)
            {
                T target = (T)item;
                result.Add(target.Title.Text, target);
            }
            return result;
        }

        /// <summary>
        /// 將ListFeed的內容轉換為DataTable
        /// </summary>
        /// <param name="tableName">名稱</param>
        /// <param name="feed">返回結果</param>
        /// <returns>轉換後的結果</returns>
        private static DataTable ConvertListFeedToDataTable(string tableName, ListFeed feed)
        {
            if (string.IsNullOrEmpty(tableName))
                throw new ArgumentNullException("tableName");

            if ((feed == null) || (feed.Entries == null) || (feed.Entries.Count == 0))
                return null;

            DataTable result = new DataTable(tableName);
            int columnCount = 0;

            /// 填充列名稱
            foreach (ListEntry.Custom element in ((ListEntry)feed.Entries[0]).Elements)
            {
                string columnName = element.XmlName;
                result.Columns.Add(columnName, typeof(string));     // 統一處理為string
                columnCount++;
            }

            // 填充資訊
            foreach (ListEntry row in feed.Entries)
            {
                string[] data = new string[columnCount];
                int i = 0;
                foreach (ListEntry.Custom element in row.Elements)
                    data[i++] = element.Value;
                result.Rows.Add(data);
            }

            return result;
        }

        #endregion

        #region Obsoleted Methods

        /// <summary>
        /// 刪除指定Worksheet
        /// </summary>
        /// <param name="entry"></param>
        /// <remarks>當前版本執行失效</remarks>
        public void RemoveWorksheet(WorksheetEntry entry)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            entry.Delete();
        }

        /// <summary>
        /// 更新Worksheet的Metadata
        /// </summary>
        /// <param name="entry">WorksheetEntry</param>
        /// <param name="title">標題</param>
        /// <param name="rows">行數</param>
        /// <param name="cols">列數</param>
        /// <remarks>當前版本執行失效</remarks>
        public void UpdateWorksheetMetaData(WorksheetEntry entry,
            string title, uint rows, uint cols)
        {
            if (entry == null)
                throw new ArgumentNullException("entry");
            entry.Title.Text = title;
            entry.Rows = rows;
            entry.Cols = cols;
            entry.Update();
        }

        #endregion
    }
}