using System;
using System.Collections.Generic;
using Google.GData.Client;
using System.Data;
using Google.GData.Spreadsheets;
using Microsoft.VisualStudio.TestTools.UnitTesting;
namespace MarvellousWorks.Training.GSpreadsheets
{
    /// <summary>
    /// GSpreadsheetsGateway的單元測試類型
    /// </summary>
    [TestClass]
    public class GSpreadsheetsGatewayFixture
    {
        #region TestInitialize

        GSpreadsheetsGateway gateway;

        [TestInitialize]
        public void Init()
        {
            string ServiceName = "marvellous-pet-1";
            string userName = "<google account>";
            string password = "<password>";
            gateway = new GSpreadsheetsGateway(
                ServiceName, userName, password);
        }

        #endregion

        #region Spreadsheet

        /// <summary>
        /// 測試載入Spreadsheets
        /// </summary>
        [TestMethod]
        public void TestGetSpreadsheets()
        {
            IDictionary<string, SpreadsheetEntry> result =
                gateway.GetSpreadsheets();
            Assert.IsNotNull(result);
            //因為至少包含menagerie範例資訊
            Assert.IsTrue(result.Count > 0);
        }

        /// <summary>
        /// 測試載入指定Spreadsheet
        /// </summary>
        [TestMethod]
        public void TestGetSpreadsheet()
        {
            string expected = "menagerie";
            SpreadsheetEntry result = gateway.GetSpreadsheet(expected);
            Assert.IsNotNull(result);
            Assert.AreEqual<string>(expected, result.Title.Text);
        }

        #endregion

        #region Worksheet

        /// <summary>
        /// 測試載入指定Spreadsheet下的所有Worksheet
        /// </summary>
        [TestMethod]
        public void TestGetWorksheets()
        {
            SpreadsheetEntry entry =
                gateway.GetSpreadsheet("menagerie");
            IDictionary<string, WorksheetEntry> result =
                gateway.GetWorksheets(entry);
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ContainsKey("Pet"));
            Assert.IsTrue(result.ContainsKey("Event"));
        }

        /// <summary>
        /// 測試載入指定Service下所有Worksheet
        /// </summary>
        [TestMethod]
        public void TestGetAllWorksheets()
        {
            IDictionary<string, WorksheetEntry> result = gateway.GetAllWorksheets();
            Assert.IsNotNull(result);
            Assert.IsTrue(result.ContainsKey("Pet"));
            Assert.IsTrue(result.ContainsKey("Event"));
        }

        /// <summary>
        /// 測試訪問指定Worksheet
        /// </summary>
        [TestMethod]
        public void TestGetWorksheet()
        {
            string spreadsheetTitle = "menagerie";
            string worksheetTitle = "Pet";
            WorksheetEntry result = gateway.GetWorksheet(
                spreadsheetTitle, worksheetTitle);
            Assert.IsNotNull(result);
            Assert.AreEqual<string>(worksheetTitle, result.Title.Text);
        }

        /// <summary>
        /// 測試增加並刪除一個新的Worksheet
        /// </summary>
        /// <remarks>
        /// 當前Google Spreadsheets API (1.2.1.0)允許在一個Spreadsheet下反覆
        /// 建立重名的Worksheet,因此為了保證單元測試的正確性，需要在執行前首
        /// 先手工清理'menagerie'下名為'temp'的各個worksheet
        /// </remarks>
        [TestMethod]
        public void TestAddWorksheet()
        {
            SpreadsheetEntry spreadsheet = gateway.GetSpreadsheet("menagerie");

            // 增加
            string tempTitle = "temp";
            uint rows = 10;
            uint cols = 20;
            WorksheetEntry entry = gateway.AddWorksheet(spreadsheet,
                tempTitle, rows, cols);

            // 檢查執行效果
            WorksheetEntry queryEntry = gateway.GetWorksheet(spreadsheet, tempTitle);
            Assert.IsNotNull(queryEntry);
            Assert.AreEqual<string>(tempTitle, queryEntry.Title.Text);
            Assert.AreEqual<uint>(rows, queryEntry.Rows);
            Assert.AreEqual<uint>(cols, queryEntry.Cols);
        }


        /// <summary>
        /// 測試訪問Worksheet的內容，並將它作為通用的DataTable返回
        /// </summary>
        [TestMethod]
        public void TestGetWorksheetContent()
        {
            DataTable result = gateway.GetWorksheetContent("menagerie", "Pet");
            DbTraceHelper.TraceData(result);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(DataTable));
            Assert.IsTrue(result.Rows.Count > 0);
            Assert.IsTrue(result.Columns.Count > 0);
        }

        /// <summary>
        /// 測試向Worksheet追加記錄
        /// </summary>
        [TestMethod]
        public void TestAddRow()
        {
            string[] localNames = new string[] { "name", "owner", "species", "sex" };
            string[] values = new string[4] { "temp", "temp", "temp", "temp" };
            gateway.AddRow("menagerie", "Pet", localNames, values);
        }

        /// <summary>
        /// 測試訪問Spreadsheet各個Worksheet的內容，並將它作為通用
        /// 的資訊容器類型DataSet返回
        /// </summary>
        [TestMethod]
        public void TestGetSpreadsheetContent()
        {
            SpreadsheetEntry entry = gateway.GetSpreadsheet("menagerie");
            DataSet result = gateway.GetSpreadsheetContent(entry);
            DbTraceHelper.TraceData(result);
            Assert.IsNotNull(result);
            Assert.IsInstanceOfType(result, typeof(DataSet));
            Assert.IsTrue(result.Tables.Count >= 2);
            Assert.IsNotNull(result.Tables["Pet"]);
            Assert.IsNotNull(result.Tables["Event"]);
        }

        #endregion
    }
}
