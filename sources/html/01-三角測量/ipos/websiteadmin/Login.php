<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>網站後台管理---www.erint.com.tw</title>
<META NAME="robots" CONTENT="noindex,nofollow" />
<link href="css/admin_style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="js/JS_CheckForm.js"></script>
<script language="JavaScript">
function checkform(theform){
if(!CheckEmpty(login.admin, "帳號")) return false;
if(!CheckEmpty(login.UserPassword, "密碼")) return false;
if(!CheckEmpty(login.passcode, "驗證碼")) return false;
}
</script>
</head>

<body>
<!--top-->
<div id="topmain"></div>
<!--top-->
<!--body-->
<div id="login_bg">
<div class="left">
<div class="comn">
  <a href="http://www.erint.com.tw" target="_blank"><img src="css/img/eirnt-LOGO.gif" alt="電接國際股份有限公司" width="207" height="66" border="0" /></a>
<h5>我們提供最好的網頁設計服務，降低您的網頁設計預算花費！ 

提供全新的網頁設計從現在開始... 
</h5>
<p class="logintxt"><span class="blogin">經驗豐富的網頁設計</span> - 我們在業界創建中小型企業網站達數年之久。</p>

<p class="logintxt"><span class="blogin">獨特的網頁設計</span> - 我們可以客製化設計出各類型的網頁設計，從簡單的網頁到複雜的電子商務。</p>

<p class="logintxt"><span class="blogin">領先的網頁設計</span> - 擁有專業團隊的網頁設計師，及程式開發團隊。</p>
</p>
</div>
</div>

<div class="left_1">
<div class="login">
<form name="login" method="POST" action="chklogin.php"  onSubmit="return checkform(this)">
<p class="login_txt_bt">網站管理員後台登入</p>

<p><span class="contacttext">帳　號：</span><input name="admin" type="text"   id="admin"  class="inp"/></p>
<p><span class="contacttext">密　碼：</span><input name="UserPassword" type="password"   id="UserPassword" class="inp"/>
</p>
<p><span class="contacttext">驗證碼：</span><input name="passcode" type="text"   id="passcode" class="inpcode"/>
<label><span class="code"><img src="../imagebuilder.php" border="1"></span> </label>
</p>
<p><input type="submit" name="button" id="button"  class="input_bot" value="登入" /> <input name="Submit2"  type="reset" class="input_bot" value="清 除" />
</p>
 
 
</form>
</div>
<div class="welcome"><img src="css/img/login-wel.gif" border="0" /></div>
</div>

</div>
<!--body-->
<!--buttom-->
<div id="login-buttom-bg"><span class="login-buttom-txt">Copyright © www.erint.com.tw All Rights Reserved. 版權所有 本系統所有內容及圖片均不得以任何型式，予以重製或傳送</span></div>
<!--buttom-->



</body>
</html>
