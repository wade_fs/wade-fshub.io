<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>網站後台管理---www.erint.com.tw</title>
<META NAME="robots" CONTENT="noindex,nofollow" />

<script src="js/prototype.lite.js" type="text/javascript"></script>
<script src="js/moo.fx.js" type="text/javascript"></script>
<script src="js/moo.fx.pack.js" type="text/javascript"></script>
<style>
body {
	font:12px Arial, Helvetica, sans-serif;
	color: #000;
	background-color: #EEF2FB;
	margin: 0px;
}
#container {
	width: 182px;
}
H1 {
	font-size: 12px;
	margin: 0px;
	width: 182px;
	cursor: pointer;
	height: 30px;
	line-height: 20px;	
}
H1 a {
	display: block;
	width: 182px;
	color: #000;
	height: 30px;
	text-decoration: none;
	moz-outline-style: none;
	background-image: url(css/img/menu_bgs.gif);
	background-repeat: no-repeat;
	line-height: 30px;
	text-align: center;
	margin: 0px;
	padding: 0px;
}
.content{
	width: 182px;
	height: 26px;
	
}
.MM ul {
	list-style-type: none;
	margin: 0px;
	padding: 0px;
	display: block;
}
.MM li {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	list-style-type: none;
	display: block;
	text-decoration: none;
	height: 26px;
	width: 182px;
	padding-left: 0px;
}
.MM {
	width: 182px;
	margin: 0px;
	padding: 0px;
	left: 0px;
	top: 0px;
	right: 0px;
	bottom: 0px;
	clip: rect(0px,0px,0px,0px);
}
.MM a:link {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(css/img/menu_bg1.gif);
	background-repeat: no-repeat;
	height: 26px;
	width: 182px;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
	text-decoration: none;
}
.MM a:visited {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(css/img/menu_bg1.gif);
	background-repeat: no-repeat;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	height: 26px;
	width: 182px;
	text-decoration: none;
}
.MM a:active {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	color: #333333;
	background-image: url(css/img/menu_bg1.gif);
	background-repeat: no-repeat;
	height: 26px;
	width: 182px;
	display: block;
	text-align: center;
	margin: 0px;
	padding: 0px;
	overflow: hidden;
	text-decoration: none;
}
.MM a:hover {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height: 26px;
	font-weight: bold;
	color: #006600;
	background-image: url(css/img/menu_bg2.gif);
	background-repeat: no-repeat;
	text-align: center;
	display: block;
	margin: 0px;
	padding: 0px;
	height: 26px;
	width: 182px;
	text-decoration: none;
}
</style>
</head>

<body>
<table width="100%" height="280" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF2FB">
  <tr>
    <td width="182" valign="top"><div id="container">
      <h1 class="type"><a href="javascript:void(0)">網站基本管理</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="css/img/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
        <li><a href="admin/main.php" target="mainFrame">管理員</a></li>
          <li><a href="baseinfo/baseinfo.php" target="mainFrame">基本管理</a></li>
           <li><a href="baseinfo/webad.php" target="mainFrame">AD管理</a></li>
           <li><a href="company/content.php?fileid=web_AboutUs&idname=關於我們" target="mainFrame">關於我們</a></li>
           <li><a href="company/content.php?fileid=web_vision&idname=願景與目標" target="mainFrame">願景與目標</a></li>
           <li><a href="company/content.php?fileid=web_service&idname=服務內容" target="mainFrame">服務內容</a></li>
           <li><a href="news/main.php?categoryid=1&idname=新聞公告" target="mainFrame">新聞公告</a></li>
           <li><a href="news/main.php?categoryid=2&idname=文件下載" target="mainFrame">文件下載</a></li>
           <li><a href="news/main.php?categoryid=3&idname=外勞申請" target="mainFrame">外勞申請</a></li>
           <li><a href="news/main.php?categoryid=4&idname=相關連結" target="mainFrame">相關連結</a></li>
           <li><a href="news/main.php?categoryid=5&idname=相關政策" target="mainFrame">相關政策</a></li>
           <li><a href="news/main.php?categoryid=6&idname=非勞工行政資源" target="mainFrame">非勞工行政資源</a></li>
           <li><a href="company/content.php?fileid=web_treaty&idname=註冊條約" target="mainFrame">註冊條約</a></li>
           <li><a href="company/content.php?fileid=web_privacy&idname=隱私權" target="mainFrame">隱私權</a></li>  
           <li><a href="company/content.php?fileid=web_contactus&idname=聯絡我們" target="mainFrame">聯絡我們</a></li>  
           
                    
           
           
          <li><a href="member/main.php" target="mainFrame">Member</a></li>
          
          <li><a href="company/content.php?fileid=web_Terms&idname=Terms" target="mainFrame">Terms</a></li>
          <li><a href="company/content.php?fileid=web_Testimonials&idname=Testimonials" target="mainFrame">Testimonials</a></li>
          <li><a href="company/content.php?fileid=web_UpgradePrice&idname=UpgradePrice" target="mainFrame">UpgradePrice</a></li>
          <li><a href="company/content.php?fileid=web_Why&idname=FAQs" target="mainFrame">FAQs</a></li>
          <li><a href="company/content.php?fileid=Welcome&idname=Welcome" target="mainFrame">Welcome</a></li>
          <li><a href="news/main.php?categoryid=1&idname=News" target="mainFrame">News</a></li>
          <li><a href="news/main.php?categoryid=2&idname=Promotion" target="mainFrame">Promotion Area</a></li> 

          
          
          
          
         </ul>
         
      </div>
      <h1 class="type"><a href="javascript:void(0)">FUNDS</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="css/img/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="Funds/nwe.php?idname=Add Funds" target="mainFrame">Add Funds</a></li>
          <li><a href="Funds/main.php?idname=Add Funds" target="mainFrame">All Activity</a></li>
         </ul>
      </div>
      <h1 class="type"><a href="javascript:void(0)">Build an app</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="css/img/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="product/main.php?categoryid=1" target="mainFrame">模板發佈</a></li>
          <li><a href="product/main.php?categoryid=2" target="mainFrame">附加產品</a></li>          
          <li><a href="product/Order.php" target="mainFrame">Order</a></li>  
          <li><a href="product/Feedback.php" target="mainFrame">Feedback</a></li> 
        </ul>
      </div>
      <!--
      <h1 class="type"><a href="javascript:void(0)">會員管理</a></h1>
      <div class="content">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td><img src="css/img/menu_topline.gif" width="182" height="5" /></td>
          </tr>
        </table>
        <ul class="MM">
          <li><a href="member/main.asp" target="mainFrame">註冊會員</a></li>
          <li><a href="member/contract.asp?code=0" target="mainFrame">注冊協議</a></li>
          <li><a href="member/log.asp" target="mainFrame">會員日記</a></li>
         </ul>
      </div>
    </div>
    -->
        
        <script type="text/javascript">
		var contents = document.getElementsByClassName('content');
		var toggles = document.getElementsByClassName('type');
	
		var myAccordion = new fx.Accordion(
			toggles, contents, {opacity: true, duration: 400}
		);
		myAccordion.showThisHideOpen(contents[0]);
	</script>
        </td>
  </tr>
</table>
</body>
</html>
