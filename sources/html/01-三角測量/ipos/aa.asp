<HTML>
<HEAD>
</HEAD>
<BODY>
<SCRIPT Language=VBScript>
'********** ActiveX Dll Functions Description **********
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXabout()
'	Description: Display the DLL version on the screen.
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXopenport(ByVal PrinterName As String)
'	Description: Start the Windows printer spool.
'	Parameter:
'		PrinterName : String, for local printer, please specified the printer driver name.
'					  For network printer, please specified the UNC path and printer name.
'--------------------------------------------------------------------------------------------------------------------------------------------------				  
'TSCActiveX.TSCLIB.ActiveXdownloadpcx(ByVal Filename As String, ByVal ImageName As String)
'	Description: Download mono PCX graphic files to the printer
'	Parameter:
'		Filename : string; file name (including file retrieval path)
'		ImageName : string, names of files that are to be downloaded in the printer memory (Please use capital letters)
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXsetup(ByVal LabelWidth As String, ByVal LabelHeight As String, ByVal Speed As String, ByVal Density As String, ByVal Sensor As String, ByVal Vertical As String, ByVal Offset As String)
'	Description: Set up label width, label height, print speed, print density, sensor type, gap/black mark vertical distance、gap/black mark shift distance.
'	Parameter:
'		LabelWidth : string, sets up label width; unit: mm
'		LabelHeight : string, sets up label height; unit: mm
'		Speed : string, sets up print speed, (selectable print speeds vary on different printer models)
'			1.0: sets print speed at 1.0"/sec
'			1.5: sets print speed at 1.5"/sec
'			2.0: sets print speed at 2.0"/sec
'			3.0: sets print speed at 3.0"/sec
'			4.0: sets print speed at 4.0"/sec
'			6.0: sets print speed at 6.0"/sec
'			8.0: sets print speed at 8.0"/sec
'			10.0: sets print speed at 10.0"/sec
'		Density : string, sets up print density
'			0~15，the greater the number, the darker the printing
'		Sensor : string, sets up the sensor type to be used
'			0: signifies that vertical gap sensor is to be used
'			1: signifies that black mark sensor is to be used
'		Vertical : string, sets up vertical gap height of the gap/black mark; unit: mm
'		Offset : string, sets up shift distance of the gap/black mark; unit:: mm; in the case of the average label, set this parameter to be 0. 
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXformfeed()
'	Description: Skip to next page (of label); this function is to be used after setup
'	Parameter: None
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXnobackfeed()
'	Description: disable the backfeed function
'	Parameter: None
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXsendcommand(ByVal command As String)
'	Description: Sends built-in commands to the bar code printer
'	Parameter: Refer to TSPL for details
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXclearbuffer()
'	Description: Clear
'	Parameter: None
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXwindowsfont(ByVal X As Long, ByVal Y As Long, ByVal Fontheight As Long, ByVal Rotation As Long, ByVal Fontstyle As Long, ByVal Fontunderline As Long, ByVal FaceName As String, ByVal TextContent As String)
'	Description: Use Windows font to print text.
'	Parameter:
'		X: Integer, the starting point of the text along the X direction, given in points
'		Y: Integer, the starting point of the text along the Y direction, given in points
'		Fontheight : Integer, the font height, given in points.
'		Rotation :Integer, rotation in counter clockwise direction
'			0 -> 0 degree
'			90-> 90 degree
'			180-> 180 degree
'			270-> 270 degree
'		Fontstyle : Integer, font style
'			0-> Normal
'			1-> Italic
'			2-> Bold
'			3-> Bold and Italic
'		Fontunderline : Integer, font with underline
'			0-> Without underline
'			1-> With underline
'		FaceName : String, font type face. Specify the true type font name. For example: Arial, Times new Roman.
'		TextContent : String, text to be printed.
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXbarcode(ByVal X As String, ByVal Y As String, ByVal CodeType As String, ByVal Height As String, ByVal Readable As String, ByVal Rotation As String, ByVal Narrow As String, ByVal Wide As String, ByVal Code As String) 
'	Description: Use built-in bar code formats to print
'	Parameter:
'		X: string; the starting point of the bar code along the X direction, given in points (of 200 DPI, 1 point=1/8 mm; of 300 DPI, 1point=1/12 mm)
'		Y: string; the starting point of the bar code along the Y direction, given in points (of 200 DPI, 1 point=1/8 mm; of 300 DPI, 1 point=1/12 mm)
'		CodeType : string
'			128 : 		Code 128, switching code subset A, B, C automatically
'			128M : 		Code 128, switching code subset A, B, C manually.
'			EAN128 : 	Code 128, switching code subset A, B, C automatically
'			25 : 		Interleaved 2 of 5
'			25C : 		Interleaved 2 of 5 with check digits
'			39 : 		Code 39
'			39C : 		Code 39 with check digits
'			93 : 		Code 93
'			EAN13 : 	EAN 13
'			EAN13+2 : 	EAN 13 with 2 digits add-on
'			EAN13+5 : 	EAN 13 with 5 digits add-on
'			EAN8 : 		EAN 8
'			EAN8+2 : 	EAN 8 with 2 digits add-on
'			EAN8+5 : 	EAN 8 with 5 digits add-on
'			CODA :		Codabar
'			POST : 		Postnet
'			UPCA : 		UPC-A
'			UPCA+2 : 	UPC-A with 2 digits add-on
'			UPCA+5 : 	UPC-A with 5 digits add-on
'			UPCE : 		UPC-E
'			UPCE+2 : 	UPC-E with 2 digits add-on
'			UPCE+5 : 	UPC-E with 5 digits add-on
'		Height : string; sets up bar code height, given in points
'		Readable : string, sets up whether to print human recognizable interpretation (text) or not.
'			0: prints no interpretation
'			1: prints interpretation
'		Rotation : string; sets up rotation degrees
'			0: 		rotates 0 degree
'			90: 	rotates 90 degrees
'			180: 	rotates180 degrees
'			270: 	rotates 270 degrees
'		Narrow : string; sets up narrow bar ratio, refer to TSPL user's manual
'		Wide : string; sets up narrow bar ratio, refer to TSPL user's manual
'		Code : string; bar code content
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXprinterfont(ByVal X As String, ByVal Y As String, ByVal FontName As String, ByVal Rotation As String, ByVal Xmul As String, ByVal Ymul As String, ByVal Content As String)
'	Description: Use printer built-in fonts to print
'	Parameter:
'		X: string; the starting point of text (character string) along the X direction, given in points (of 200 DPI, 1 point=1/8 mm; of 300 DPI, 1 point=1/12 mm)
'		Y: string; the starting point of text (character string) along the Y direction, given in points (of 200 DPI, 1 point=1/8 mm; of 300 DPI, 1 point=1/12 mm)
'	FontName : string; built-in font type name, 12 kinds in sum
'		1: 8*/12 dots
'		2: 12*20 dots
'		3: 16*24 dots
'		4: 24*32 dots
'		5: 32*48 dots
'		TST24.BF2: Traditional Chinese 24*24
'		TST16.BF2: Traditional Chinese 16*16
'		TTT24.BF2: Traditional Chinese 24*24 (Telecommunication Code)
'		TSS24.BF2: Simplified Chinese 24*24
'		TSS16.BF2: Simplified Chinese 16*16
'		K: Japan, Korean font 24*24,
'		L: Japan Korean font 16*16
'	Rotation : string; sets up the rotation degree of the text (character string)
'		0: rotates 0 degree
'		90: rotate 90 degrees
'		180: rotate 180 degrees
'		270: rotate 270 degrees
'	Xmul : string; sets up the magnification rate of text (character string) along the X direction, range: 1~8
'	Ymul : string; sets up the magnification rate of text (character string) along the Y direction, range: 1~8
'	Content : string; prints the content of text (character string)
'
'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXprintlabel(ByVal NumberOfSet As String, ByVal NumberOfCopy As String)
'	Description: Print label content
'	Parameter:
'		NumberOfSet : string; sets up the number of label sets
'		NumberOfCopy : string, sets up the number of print copies

'--------------------------------------------------------------------------------------------------------------------------------------------------
'TSCActiveX.TSCLIB.ActiveXcloseport()
'	Description: Close Windows printer spool.
'	Parameter: None

'--------------------------------------------------------------------------------------------------------------------------------------------------
'
'********** VB SCRIPT Example **********
'
'


document.Write now
If HOUR(Now) < 12 Then
document.Write " Good morning!</P>"
Else 
document.Write " Good afternoon!</P>"
End if

document.Write "TSC ActiveX Dll Test!</P>"

Dim TSCObj 
Set TSCObj = CreateObject("TSCActiveX.TSCLIB") 

TSCObj.ActiveXabout
TSCObj.ActiveXopenport "TSC TDP-245"

'TSCObj.ActiveXdownloadpcx "G:\ASP-Example-TSCE\UL.PCX","UL.PCX"


TSCObj.ActiveXsetup "101.6","63.5","5","8","0","2","0"
'TSCObj.ActiveXformfeed
'TSCObj.ActiveXnobackfeed0
TSCObj.ActiveXsendcommand "SET TEAR ON"

TSCObj.ActiveXclearbuffer
TSCObj.ActiveXprinterfont "10","10","3","0","1","1","ActiveXdll Test!!"
'TSCObj.ActiveXsendcommand "PUTPCX 10,200,""UL.PCX"""
TSCObj.ActiveXwindowsfont 400, 200, 48, 0, 3, 1, "arial", "DEG 0"
TSCObj.ActiveXwindowsfont 400, 200, 48, 90, 3, 1, "arial", "DEG 90"
TSCObj.ActiveXwindowsfont 400, 200, 48, 180, 3, 1, "arial", "DEG 180"
TSCObj.ActiveXwindowsfont 400, 200, 48, 270, 3, 1, "arial", "DEG 270"
TSCObj.ActiveXbarcode "100", "40", "128", "50", "1", "0", "2", "2", "123456789"
TSCObj.ActiveXprintlabel "1","1"
TSCObj.ActiveXcloseport

</SCRIPT>
</BODY>
</HTML>
